#!/usr/bin/env bash

set -xeu

cd /app

chmod +x ./scripts/sql_schema.sh

# Remove previous failed deployments
rm -f /opt/wildfly/wildfly-10.1.10.Final/standalone/deployments/*.failed

# Start Wildfly Server
echo "[WebServer] Starting Server"
nohup /opt/wildfly/wildfly-10.1.0.Final/bin/standalone.sh 1>/tmp/server.log 2>/tmp/server.err &
echo "[WebServer] Server started"

# Give time for Wildfly to start
sleep 10

# Deploy MySQL connector
cp -f *.jar /opt/wildfly/wildfly-10.1.0.Final/standalone/deployments/

# Sleep some more
sleep 5

# Configure Wildfly with Database connection
./scripts/entrypoint.sh ./scripts/sql_schema.sh

# Redeploy until pass
rm -f /opt/wildfly/wildfly-10.1.10.Final/standalone/deployments/*.failed

# Deploy app
cp -f CompactDiscRESTEnterprise/target/*.war /opt/wildfly/wildfly-10.1.0.Final/standalone/deployments/
