#!/usr/bin/env bash

set -xe

cd /app

echo "Configure MySQL schema"
# If MySQL client is not installed, fail!
MYSQL=$(which mysql)
${MYSQL} -h "${DB_CONNECTIONSTRING}" -p"${DB_PASSWORD}" -u "${DB_USERNAME}" "${DB_NAME}" < schema.sql || true


if ! cat /opt/wildfly/wildfly-10.1.0.Final/standalone/configuration/standalone.xml | grep "${DB_CONNECTIONSTRING}"
then
  /opt/wildfly/wildfly-10.1.0.Final/bin/jboss-cli.sh --connect controller=localhost --command='/subsystem=datasources/data-source=MySQLDatasource: add(jndi-name="java:/mySQL", driver-name="mysql-connector-java-5.1.32-bin.jar_com.mysql.jdbc.Driver_5_1", connection-url="jdbc:mysql://'"${DB_CONNECTIONSTRING}"':3306/conygre", user-name='"${DB_USERNAME}"', password='"${DB_PASSWORD}"')'
fi
